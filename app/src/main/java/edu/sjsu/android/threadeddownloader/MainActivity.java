package edu.sjsu.android.threadeddownloader;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    ImageView image;
    EditText text;
    String url;
    ProgressDialog progress;
    Handler handler = new Handler();
    Bitmap bitmap;
    boolean isRunning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        image = (ImageView) findViewById(R.id.imageView);
        text = (EditText) findViewById(R.id.editText);
        url = "";
        bitmap = null;
        progress = new ProgressDialog(this);
        progress.setTitle("Your download will finish shortly");
    }

    Bitmap downloadBitmap(String urlS) throws IOException {
        Bitmap myBitmap = null;
        URL url = new URL(urlS);
        HttpURLConnection urlC = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = new BufferedInputStream(urlC.getInputStream());
            myBitmap = BitmapFactory.decodeStream(in);
            bitmap = myBitmap;
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Download failed", Toast.LENGTH_SHORT).show();
            isRunning = false;
        } finally {
            urlC.disconnect();
        }
        return myBitmap;
    }


    Runnable foregroundRunnable = new Runnable(){
        @Override
        public void run() {
            if (bitmap == null){
                Toast.makeText(getApplicationContext(), "Runnable download failed", Toast.LENGTH_SHORT).show();
            }
            image.setImageBitmap(bitmap);
            progress.dismiss();
        }
    };

    public void runRunnable(View view) {
        url = text.getText().toString();
        progress.setMessage("Downloading...");
        progress.show();

        Runnable runnable = () -> {
            try {
                Thread background = new Thread(() -> {
                    try {
                        downloadBitmap(url);
                    } catch(Exception e) {
                        Toast.makeText(getApplicationContext(), "Run runnable failed", Toast.LENGTH_SHORT).show();
                        isRunning = false;
                    }
                    handler.post(foregroundRunnable);
                });
                background.start();
            } catch(Exception e) {
                Toast.makeText(getApplicationContext(), "Run runnable failed", Toast.LENGTH_SHORT).show();
                isRunning = false;
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public void runMessages(View view) {
        url = text.getText().toString();
        progress.setMessage("Downloading...");
        progress.show();

        // create a thread to run in the background
        Thread background = new Thread(() -> {
            try {
                bitmap = downloadBitmap(url);
            } catch(Exception e) {
                Toast.makeText(getApplicationContext(), "Run messages failed", Toast.LENGTH_SHORT).show();
                isRunning = false;
            }
            Message msg = handler.obtainMessage(0, bitmap);
            if (isRunning) {
                handler.sendMessage(msg);
            }
        });
        background.start();
    }

    public void runAsyncTask(View view) {
        new asyncTask().execute();
    }

    public void resetImage(View view) {
        image.setImageResource(R.drawable.calico);
    }

    // AsyncTask for AsyncButton
    private class asyncTask extends AsyncTask<Void, Void, Bitmap> {
        protected void onPreExecute() {
            url = text.getText().toString();
            progress.setMessage("downloading asynchronously...");
            progress.show();
        }

        @Override
        protected Bitmap doInBackground(Void... unused) {
            try {
                bitmap = downloadBitmap(url);
                return bitmap;
            } catch(Exception e) {
                Toast.makeText(getApplicationContext(), "Async download failed", Toast.LENGTH_SHORT).show();
                isRunning = false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (bitmap == null){
                Toast.makeText(getApplicationContext(), "Async download failed", Toast.LENGTH_SHORT).show();
                isRunning = false;
            }
            image.setImageBitmap(bitmap);
            progress.dismiss();
        }
    }
}